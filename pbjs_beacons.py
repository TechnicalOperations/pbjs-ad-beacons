from browsermobproxy import Server
from selenium import webdriver
from pyvirtualdisplay import Display
import numpy
import json
import ast
import subprocess, signal
import os
import re
import csv


# Start browsermob server and proxy
def server_setup():
	server = Server("/home/mgillespie/Downloads/browsermob-proxy-2.1.4/bin/browsermob-proxy")
	server.start()
	proxy = server.create_proxy()
	return proxy

# Build webdriver settings
def browser_setup():
	profile = webdriver.FirefoxProfile()
	profile.set_proxy(proxy.selenium_proxy())
	profile.set_preference('general.useragent.override',
						   'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36')

	caps = webdriver.DesiredCapabilities.FIREFOX.copy()
	caps['webdriver_accept_untrusted_certs'] = True
	caps['webdriver_assume_untrusted_issuer'] = True

	driver = webdriver.Firefox(firefox_profile=profile)
	driver.set_page_load_timeout(15)
	return driver

# Read SiteList CSV
def csv_reader():
	csv_input = "hb2.csv"
	csv_arr = []
	with open(csv_input, 'r') as f:
		next(f)
		reader = csv.reader(f)
		for row in reader:
			csv_arr.append(row[0])
	f.close()
	return csv_arr

# Create CSV and headers
def csv_writer():
	csv_output = "adv_hb.csv"
	with open(csv_output, "w") as f:
		writer = csv.writer(f, quoting=csv.QUOTE_ALL)
		writer.writerow(["Site","AppNexus","Openx","Pubmatic",
						 "Rubicon","Criteo","Amazon","AOL",
						 "Sovrn","Yieldbot","Sonobi","Index",
						 "Proximic","Lotame","AudienceS","Adform",
						 "DFP"])
	f.close()

# Update CSV
def csv_updater(results):
	csv_output = "adv_hb.csv"
	for i in results:
		with open(csv_output, "a") as f:
			writer = csv.writer(f, quoting=csv.QUOTE_ALL)
			domain = i[0]
			writer.writerow([i[0], i[1]['AppNexus'], i[1]['Openx'], i[1]['Pubmatic'],
							i[1]['Rubicon'],i[1]['Criteo'],i[1]['Amazon'],i[1]['AOL'],
							i[1]['Sovrn'],i[1]['Yieldbot'],i[1]['Sonobi'],i[1]['Index'],
							i[1]['Proximic'],i[1]['Lotame'],i[1]['AudienceS'],i[1]['Adform'],
							i[1]['DFP']])
		f.close()
	return

# Scrape website and hand off to regex pattern matching
def scrape(domain, proxy,driver):
	domain_info = []
	domain_fqdn = "http://{}".format(domain)
	proxy.new_har(domain_fqdn, options={'captureHeaders': True})
	try:
		driver.get(domain_fqdn)
	except Exception as e:
		pass

	result = json.dumps(proxy.har, ensure_ascii=False)
	result = ast.literal_eval(result)
	x = 1
	a = []
	for i in result['log']['entries']:
		#print("URL Captured[{}]: {}".format(x, i['request']['url']))
		a.append(i['request']['url'])
		x += 1
	adv = regex_url(domain, a)
	domain_info.append(domain)
	domain_info.append(adv)
	return domain_info

# Regex pattern matching
def regex_url(domain, urls):
	print("Domain: {}".format(domain))
	adv = {}
	patterns = {'AppNexus':'\.adnxs\.com\/jpt',
				'Openx':'\.servedbyopenx\.com\/w\/1\.0\/jstag',
				'Openx':'\.servedbyopenx\.com\/w\/1\.0\/acj',
				'Openx':'\.openx\.net\/w\/1\.0\/acj',
				'Pubmatic':'\.pubmatic\.com\/AdServer\/js\/gshowad\.js',
				'Pubmatic':'\.pubmatic\.com\/AdServer\/AdCallAggregator',
                'Pubmatic':'\.pubmatic\.com\/AdServer\/AdServerServlet',
				'Rubicon':'\.rubiconproject\.com\/ad\/9707\.js',
                'Rubicon':'\.rubiconproject\.com\/a\/api\/fastlane\.json',
                'Rubicon':'\.rubiconproject\.com\/a\/api\/ads\.json',
				'Criteo':'\.criteo\.com\/delivery\/rta\/rta\.js',
				'Amazon':'\.amazon\-adsystem\.com\/aax2\/amzn_ads\.js',
                'Amazon':'\.amazon\-adsystem\.com\/e\/dtb\/bid',
				'AOL':'\.adtechus\.com\/pubapi',
				'Sovrn':'\.lijit\.com\/rtb\/bid',
				'Yieldbot':'\.yldbt.com\/js\/yieldbot\.intent\.js',
                'Yieldbot':'\.yldbt\.com\/m\/',
				'Sonobi':'\.sonobi\.com\/trinity\.js',
				'Index':'\.casalemedia\.com\/cygnus',
                'Index':'\.casalemedia\.com\/headertag',
				'Proximic':'\.zqtk\.net\/',
				'Lotame':'ad\.crwdcntrl\.net\/',
				'AudienceS':'\.revsci.net\/pql',
				'Adform':'adx\.adform\.net\/adx\/\?rp\=4',
				'DFP':'\.doubleclick\.net\/gampad\/ads'}
	for k,v in patterns.items():
		adv[k] = "False"
	for i in urls:
		for k,v in patterns.items():
			m = re.search("{}".format(v), str(i))
			if m:
				adv[k] = "True"

	return adv

# Kill PIDs for browsermob (due to known bug of server.stop() and proxy.close())
def kill_proxy():
	p = subprocess.Popen(['ps', 'a'], stdout=subprocess.PIPE)
	out, err = p.communicate()
	for line in out.splitlines():
		if 'browsermob' in str(line):
			pid = int(line.split(None, 1)[0])
			os.kill(pid, signal.SIGKILL)

# Entry point
if __name__ == '__main__':
	display = Display(visible=0, size=(800, 1000))
	display.start()
	proxy = server_setup()

	csv_arr = csv_reader()
	#csv_writer()
	na = numpy.array_split(csv_arr, 550)
	driver = browser_setup()
	for domains in na:
		comp_arr = []
		for domain in domains:
			entry = scrape(domain,proxy,driver)
			comp_arr.append(entry)
		csv_updater(comp_arr)
		print("Complete")

	driver.quit()
	display.stop()

	kill_proxy()
